{
	"name" : "SpatialBehavior",
	"version" : 1,
	"creationdate" : 3727629205,
	"modificationdate" : 3727629365,
	"viewrect" : [ 25.0, 104.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"SpatialBehavior.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"spatial_behavior.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"spatial_move.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"spatial_morph.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"set_coord.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"spatial_24.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"plot_circle.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"deg2rad.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"spatial_quad.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"midi2deg.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"code" : 		{

		}
,
		"other" : 		{

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
